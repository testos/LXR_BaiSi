//
//  LXRMeCell.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/30.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRMeCell.h"

@implementation LXRMeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //取消分割线
        self.accessoryType = UITableViewCellAccessoryNone;
        //字体
        self.textLabel.font = [UIFont systemFontOfSize:16];
        //字体颜色
        self.textLabel.textColor = [UIColor darkGrayColor];
        //设置背景图片
        UIImageView* image =[[UIImageView alloc]init];
        image.image = [UIImage imageNamed:@"mainCellBackground"];
        self.backgroundView = image;
        
    }
    return self;
}
-(void)layoutSubviews{
    
    [super layoutSubviews];
    //如果cell中没有图片就返回
    if (self.imageView.image == nil) return;
    //图片Frame设置
    self.imageView.width = 30;
    self.imageView.height = self.imageView.width;
    self.imageView.centerY = self.contentView.height*0.5;
    //文字X设置
    self.textLabel.x = CGRectGetMaxX(self.imageView.frame) + LXRTopicCellMargin;
}
@end
