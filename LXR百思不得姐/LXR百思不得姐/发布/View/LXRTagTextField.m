//
//  LXRTagTextField.m
//  01- 百思不得姐
//
//  Created by MACBOOK on 16/7/17.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRTagTextField.h"

@implementation LXRTagTextField

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.placeholder = @"多个标签用逗号或者换行隔开";
        //设置了占位文字内容,才能设置占位文字的颜色
        [self setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        
        self.height = LXRTagH;
    }
    return self;
}

- (void)deleteBackward{
    !self.deleteBlock ? : _deleteBlock();
    [super deleteBackward];
}


/**
 *  也能在这个方法中监听键盘的输入,比如输入换行
 */
//-(void)insertText:(NSString *)text{
//    [super insertText:text];
//    
//}
@end
