//
//  LXRTagTextField.h
//  01- 百思不得姐
//
//  Created by MACBOOK on 16/7/17.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LXRTagTextField : UITextField
/** 按了删除按钮回调 */
@property(nonatomic,copy)void(^deleteBlock)();
@end
