//
//  LXRPostWordViewController.m
//  01- 百思不得姐
//
//  Created by mac on 16/7/1.
//  Copyright © 2016年 mac. All rights reserved.
//  发段子控制器

#import "LXRPostWordViewController.h"
#import "LXRPlaceholderTextView.h"
#import "LXRAddTagToolbar.h"

@interface LXRPostWordViewController ()<UITextViewDelegate>
/** 文本输入控件 */
@property (nonatomic, strong) LXRPlaceholderTextView *textView;
/** 工具条 */
@property (nonatomic, weak) LXRAddTagToolbar *toolbar;
@end

@implementation LXRPostWordViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //先退出之前的键盘(防止下一个界面输入一半不点确认,弹不出加号View)
    [self.view endEditing:YES];
    //界面显示完开始就谈键盘,增加用户体验
    [self.textView becomeFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor redColor];
    /** 初始化导航栏设置 */
    [self setupNav];
    /** TextView */
    [self setupTextView];
    /** setupToolbar */
    [self setupToolbar];
}
/** Toolbar工具条 */
-(void)setupToolbar{
    LXRAddTagToolbar* toolbar = [LXRAddTagToolbar viewFromXib];
    toolbar.width = self.view.width;
    toolbar.y = self.view.height - toolbar.height;
    [self.view addSubview:toolbar];
    self.toolbar = toolbar;
    //监听键盘将要改变时通知
    [LXRNoteCenter addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
}
#pragma mark 监听键盘改变时调用 修改Frame
-(void)keyboardWillChangeFrame:(NSNotification*)note{
    //键盘最终的Frame
    CGRect keyboardF = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    //键盘退出时间(取出时间,完成跟系统键盘退出时间一样)
    CGFloat duration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey]doubleValue];
    
    //动画退出工具条
    [UIView animateWithDuration:duration animations:^{
        //(键盘的Y值-用屏幕的高度)->计算->工具条的transform
        self.toolbar.transform = CGAffineTransformMakeTranslation(0, keyboardF.origin.y - LXRScreenH);
    }];
}
/** 初始化导航栏设置 */
-(void)setupNav{
    self.title = @"发表文字";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:UIBarButtonItemStyleDone target:self action:@selector(Back)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"发表" style:UIBarButtonItemStyleDone target:self action:@selector(Post)];
    self.navigationItem.rightBarButtonItem.enabled = NO;//默认不能点击
    //强制刷新
    [self.navigationController.navigationBar layoutIfNeeded];
    
}

 /** TextView */
-(void)setupTextView{
    self.automaticallyAdjustsScrollViewInsets = YES;
    LXRPlaceholderTextView* textView = [[LXRPlaceholderTextView alloc]init];
    textView.frame = self.view.bounds;
    textView.placeholder = @"把好玩的图片，好笑的段子或糗事发到这里，接受千万网友膜拜吧！发布违反国家法律内容的，我们将依法提交给有关部门处理。";
    textView.delegate = self;
    [self.view addSubview:textView];
    self.textView = textView;
    
}
/** 发表 */
-(void)Post{
    LXRLogFunc;
}
/** 返回 */
-(void)Back{

    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - <UITextViewDelegate>
/**文字改变*/
-(void)textViewDidChange:(UITextView *)textView{
    /*当文字改变,监听发表按钮enabled属性,实现能不能点击*/
    self.navigationItem.rightBarButtonItem.enabled = textView.hasText;
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}
@end
