//
//  LXRPublishViewController.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/25.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRPublishViewController.h"
#import "LXRAutoLoginButton.h"
#import "LXRPostWordViewController.h"
#import "LXRNavController.h"

/**第三方*/
#import "POP.h"
/** 动画延迟时间*/
static CGFloat const LXRAnimationDelay = 0.1;
/** 动画效果设置*/
static CGFloat const LXRSpringFactor = 10;

@interface LXRPublishViewController ()
/** 按钮图片数据 */
@property(nonatomic,strong)NSArray* images;
/** 按钮文字数据 */
@property(nonatomic,strong)NSArray* titles;

@end

@implementation LXRPublishViewController
#pragma mark - 懒加载
-(NSArray *)titles{
    
    if (_titles == nil) {
        _titles = @[@"发视频", @"发图片", @"发段子", @"发声音", @"审帖", @"离线下载"];;
    }
    return _titles;
}
-(NSArray *)images{
    
    if (_images == nil) {
        _images = @[@"publish-video", @"publish-picture", @"publish-text", @"publish-audio", @"publish-review", @"publish-offline"];;
    }
    return _images;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //当进来这个界面,首先让整个View不可以点击
    self.view.userInteractionEnabled = NO;
    
    //添加按钮
    [self addButtons];
    
    //添加标语
    [self addSloganView];

}

#pragma mark - pop和CoreAnimation的区别
#if 0
pop和CoreAnimation的区别:
1.CoreAnimation的动画只能添加到Layer上
2.pop的动画能添加到任何对象上
3.pop的底层并非基于CoreAnimation,是基于CADisplayLink
4.CoreAnimation的动画仅仅是表象,并不会真正修改对象的Frame/Size等值
5.pop的动画实时修改对象的属性,真正地修改了对象的属性
#endif
/**pop简介*/
-(void)POPintro{
//    //kPOPViewCenter是根据 View 中心点进行动画
//    POPSpringAnimation* ani = [POPSpringAnimation animationWithPropertyNamed:kPOPViewCenter];
//    //动画开始时间(拿到当前时间+自定义时间)
//    ani.beginTime = CACurrentMediaTime() + 1.0;
//    //弹簧效果设置
//    ani.springBounciness = 20;//弹力效果模式是4,范围是[0.20]
//    ani.springSpeed = 20;     //速度模式是12,范围是[0,20]
//    //包装View开始值--->CGPoint
//    ani.fromValue = [NSValue valueWithCGPoint:CGPointMake(self.sloganView.centerX, self.sloganView.centerY)];
//    //包装View最终值--->CGPoint
//    ani.toValue = [NSValue valueWithCGPoint:CGPointMake(self.sloganView.centerX, 400)];
//    //添加动画  key可是储存值,方便查找和释放
//    [self.sloganView pop_addAnimation:ani forKey:nil];
//    
//    
//    //kPOPLayerPositionY是根据 Layer 中心点进行动画
//    POPSpringAnimation* ani = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
//    //动画开始时间(拿到当前时间+自定义时间)
//    ani.beginTime = CACurrentMediaTime() + 1.0;
//    //弹簧效果设置
//    ani.springBounciness = 20;//弹力效果模式是4,范围是[0.20]
//    ani.springSpeed = 20;     //速度模式是12,范围是[0,20]
//    //包装Layer改变范围
//    ani.fromValue = @(self.sloganView.layer.position.y);
//    ani.toValue = @(400);
//    //添加动画  key可是储存值,方便查找和释放
//    [self.sloganView pop_addAnimation:ani forKey:nil];
}

#pragma mark - 添加按钮
-(void)addButtons{

    //中间的6个按钮
    int maxCols = 3;//每行最大个数
    CGFloat btnW = 72;//图片本身的宽度
    CGFloat btnH = btnW+30;//30是文字的高度
    CGFloat buttonStartY = (LXRScreenH - 2 * btnH) * 0.5;
    CGFloat buttonSatarX = 25;
    CGFloat xMargin = (LXRScreenW - 2 * buttonSatarX - maxCols * btnW)/(maxCols - 1);
    
    for (int i = 0; i<self.images.count; i++) {
        LXRAutoLoginButton* btn = [[LXRAutoLoginButton alloc]init];
        //按钮添加点击事件
        [btn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = i;
        [self.view addSubview:btn];
        
        //设置内容
        [btn setTitle:self.titles[i] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:self.images[i]] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        
        //设置Frame
        int col = i/maxCols;
        int row = i%maxCols;
        CGFloat buttonX = buttonSatarX + (row)*(xMargin+btnW);
        CGFloat buttonEndY = buttonStartY + (col)*btnH;
        CGFloat buttonStartY = buttonEndY - LXRScreenH;//让按钮开始位置在屏幕上边
        
        //添加动画
        POPSpringAnimation* ani = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
        //设置动画改变值范围
        ani.fromValue = [NSValue valueWithCGRect:CGRectMake(buttonX, buttonStartY, btnW, btnH)];
        ani.toValue = [NSValue valueWithCGRect:CGRectMake(buttonX, buttonEndY, btnW, btnH)];
        //设置弹簧速度和弹力
        ani.springSpeed = LXRSpringFactor;
        ani.springBounciness = LXRSpringFactor;
        //设置动画开始时间,按钮分开动画
        ani.beginTime = CACurrentMediaTime() + LXRAnimationDelay*i;
        //动画结束
        [ani setCompletionBlock:^(POPAnimation *ani, BOOL finished) {
            //标语动画执行完毕,恢复View可以点击
            self.view.userInteractionEnabled = YES;
        }];
        //按钮添加动画
        [btn pop_addAnimation:ani forKey:nil];
    }
}

-(void)cancelWithEndAnimation:(void(^)())complentionBlock{
    
    //让控制器View不能点击
    self.view.userInteractionEnabled = NO;
    //子控件索引从第2个开始才是按钮
    int beginIndex = 2;
    
    for (int i = beginIndex; i<self.view.subviews.count; i++) {
        //取出当前View
        UIView* subView = self.view.subviews[i];
        //POP基本动画
        POPBasicAnimation* ani = [POPBasicAnimation animationWithPropertyNamed:kPOPViewCenter];
        CGFloat centerY =subView.centerY+LXRScreenH;
        //动画执行节奏(一开始很慢,后面加快)
        ani.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        ani.toValue = [NSValue valueWithCGPoint:CGPointMake(subView.centerX, centerY)];
        ani.beginTime = CACurrentMediaTime()+(i-beginIndex)*LXRAnimationDelay;
        [subView pop_addAnimation:ani forKey:nil];
        //监听最后一个动画
        if (i == self.view.subviews.count -1) {
            [ani setCompletionBlock:^(POPAnimation *ani, BOOL finished) {
                //跳转返回
                [self dismissViewControllerAnimated:NO completion:nil];
                
                //执行传进来的complentionBlock参数
                !complentionBlock? : complentionBlock();
            }];
        }
    }
}
#pragma 点击按钮
-(void)buttonClick:(UIButton*)button{
    
    //当动画结束之后
    [self cancelWithEndAnimation:^{
        if (button.tag == 0) {
            LXRLog(@"发视频");
        }else if (button.tag == 1){
            LXRLog(@"发图片");
        }else if (button.tag == 2){
            //LXRLog(@"发段子");
            LXRPostWordViewController* postWord = [[LXRPostWordViewController alloc]init];
            LXRNavController* nav = [[LXRNavController alloc]initWithRootViewController:postWord];
        
            //这里不能使用self来弹出其他控制器,因为self执行了dismiss操作
            UIViewController* vc = [UIApplication sharedApplication].keyWindow.rootViewController;
            [vc presentViewController:nav animated:YES completion:nil];
            
        }else if (button.tag == 3){
            LXRLog(@"发声音");
        }else if (button.tag == 4){
            LXRLog(@"审帖");
        }else if (button.tag == 5){
            LXRLog(@"离线下载");
        }
    }];
}

#pragma mark - 添加标语
-(void)addSloganView{
    //添加标语
    UIImageView* sloganView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"app_slogan"]];
    [self.view addSubview:sloganView];
    
    //标语动画
    POPSpringAnimation* SloganAni = [POPSpringAnimation animationWithPropertyNamed:kPOPViewCenter];
    CGFloat centerX = LXRScreenW*0.5;//水平居中
    CGFloat centerEndY = LXRScreenH*0.2;//屏幕高度五分之一
    CGFloat centerStartY = centerEndY - LXRScreenH;//开始Y值在屏幕外
    sloganView.centerY = centerStartY;
    
    SloganAni.fromValue = [NSValue valueWithCGPoint:CGPointMake(centerX,centerStartY)];//开始位置
    SloganAni.toValue = [NSValue valueWithCGPoint:CGPointMake(centerX,centerEndY)];//最终位置
    
    SloganAni.beginTime = CACurrentMediaTime() + self.images.count * LXRAnimationDelay;//开始时间
    
    SloganAni.springSpeed = LXRSpringFactor;
    SloganAni.springBounciness = LXRSpringFactor;
    [sloganView pop_addAnimation:SloganAni forKey:nil];
}
#pragma mark - 点击取消按钮
- (IBAction)Cancel {
    [self cancelWithEndAnimation:nil];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self cancelWithEndAnimation:nil];
}


@end
