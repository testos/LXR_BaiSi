//
//  LXRNewViewController.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/6.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRNewViewController.h"

@interface LXRNewViewController ()

@end

@implementation LXRNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置导航栏标题
    self.navigationItem.titleView  = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"MainTitle"]];
    //添加左item
    self.navigationItem.leftBarButtonItem  =[UIBarButtonItem itemWithImage:@"MainTagSubIcon" highImage:@"MainTagSubIconClick" target:self action:@selector(ClickBtn)];
    //设置背景色
    self.view.backgroundColor = LXR_BJ_Color;
}

-(void)ClickBtn{
    
    LXRLogFunc;
}

@end
