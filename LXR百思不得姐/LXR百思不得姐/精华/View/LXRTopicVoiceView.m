//
//  LXRTopicVoiceView.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/27.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRTopicVoiceView.h"
#import "LXRTopicModel.h"
#import "LXRShowPictureViewController.h"
#import "UIImageView+WebCache.h"

@interface LXRTopicVoiceView ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *voicetimeLable;
@property (weak, nonatomic) IBOutlet UILabel *voiceCountLable;

@end

@implementation LXRTopicVoiceView

-(void)awakeFromNib{
    //当设置好Frame,打印结果与设置无问题的时候,达不到预期的效果
    //首先考虑是系统的自动调整属性-->设置不用自动调整
    self.autoresizingMask = UIViewAutoresizingNone;
    
    //给图片添加监听器
    self.imageView.userInteractionEnabled = YES;
    [self.imageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showPicture)]];
    
    
    
}
/** 当点击图片时*/
//点击查看按钮时每反应,解决方法让点击查看按钮不能点击
-(void)showPicture{
    LXRShowPictureViewController* showPicture = [[LXRShowPictureViewController alloc]init];
    showPicture.model = self.model;
    //模态跳转  让当前不是控制器时,调用窗口根控制器来执行模态跳转
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:showPicture animated:YES completion:nil];
}

-(void)setModel:(LXRTopicModel *)model
{
    _model = model;
    
    //图片
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.large_image]];
    //播放次数
    self.voiceCountLable.text = [NSString stringWithFormat:@"%zd播放",model.playcount];
    //播放时长
    NSInteger minute = model.voicetime/60;
    NSInteger second = model.voicetime%60;
    self.voicetimeLable.text = [NSString stringWithFormat:@"%02zd:%02zd",minute,second];
}




@end
