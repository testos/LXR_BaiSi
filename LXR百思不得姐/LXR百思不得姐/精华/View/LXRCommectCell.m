//
//  LXRCommectCell.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/29.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRCommectCell.h"
#import "LXRCommentModel.h"
#import "LXRUserModel.h"

#import "UIImageView+WebCache.h"

@interface LXRCommectCell ()
/**头像*/
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
/**性别*/
@property (weak, nonatomic) IBOutlet UIImageView *SexImageView;
/**用户名*/
@property (weak, nonatomic) IBOutlet UILabel *userNameLable;
/**评论内容*/
@property (weak, nonatomic) IBOutlet UILabel *contentLable;
/**点赞次数*/
@property (weak, nonatomic) IBOutlet UILabel *likeLable;
/**音频*/
@property (weak, nonatomic) IBOutlet UIButton *voiceButton;

@end


@implementation LXRCommectCell
#pragma mark - UIMenuController需要重写的方法
/** 可以成为第一响应者*/
-(BOOL)canBecomeFirstResponder{
    return YES;
}
/** 不执行系统默认的事件(比如菜单:剪切/复制/选中等)*/
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    return NO;
}

/** 读Xib时调用 */
- (void)awakeFromNib {
    UIImageView* bgView = [[UIImageView alloc]init];
    bgView.image = [UIImage imageNamed:@"mainCellBackground"];
    self.backgroundView = bgView;
    
}
/** 模型-->控件之间赋值 */
-(void)setModel:(LXRCommentModel *)model{
    _model = model;
    
    //模型控件赋值

    //设置圆形头像
    [self.profileImageView setCircleHeader:model.user.profile_image];
    
    self.SexImageView.image = [model.user.sex isEqualToString:LXRUserSexMale]?[UIImage imageNamed:@"Profile_manIcon"]:[UIImage imageNamed:@"Profile_womanIcon"];
    self.userNameLable.text = model.user.username;
    self.contentLable.text = model.content;
    self.likeLable.text = [NSString stringWithFormat:@"%zd",model.like_count];
    
    /**处理音频按钮**/
    if (model.voiceuri.length) {//如果字符串有长度   服务器返回的@"",所以要用长度判断
        self.voiceButton.hidden = NO;
        [self.voiceButton setTitle:[NSString stringWithFormat:@"%zd",model.voicetime] forState:UIControlStateNormal];
    }else{
        self.voiceButton.hidden = YES;
    }
}
/** 重新设置Frame 宽和X值*/
-(void)setFrame:(CGRect)frame{
    //设置Frame  版本更新 无需设置
    //frame.origin.x = LXRTopicCellMargin;
    //frame.size.width = LXRScreenW - 2*LXRTopicCellMargin;
    
    [super setFrame:frame];
}

@end
