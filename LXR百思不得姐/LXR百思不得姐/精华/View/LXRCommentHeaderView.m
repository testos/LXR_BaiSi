//
//  LXRCommentHeaderView.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/28.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRCommentHeaderView.h"

@interface LXRCommentHeaderView ()
/** 文字标签 */
@property(nonatomic,weak)UILabel* lable;

@end
@implementation LXRCommentHeaderView
/**根据TableView创建头部视图*/
+(instancetype)headerViewWithTableView:(UITableView *)tableView{
    //循环引用头部视图
    static NSString* ID = @"header";
    LXRCommentHeaderView* header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if (!header) {
        header = [[LXRCommentHeaderView alloc]initWithReuseIdentifier:ID];
    }
    return header;
}

/**创建LXRCommentHeaderView内部自动创建文字Lable*/
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = LXR_BJ_Color;
        //创建Lable
        UILabel* lable = [[UILabel alloc]init];
        lable.font = [UIFont systemFontOfSize:15];
        lable.textColor = LXR_RGB_Color(67, 67, 67);
        lable.width = 200;
        lable.x = LXRTopicCellMargin;
        lable.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:lable];
        self.lable = lable;
    }
    
    
    return self;
}

-(void)setTitle:(NSString *)title{
    _title = [title copy];
    self.lable.text = _title;
}


@end
