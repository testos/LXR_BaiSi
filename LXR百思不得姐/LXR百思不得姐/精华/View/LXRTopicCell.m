//
//  LXRTopicCell.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/21.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRTopicCell.h"
#import "UIImageView+WebCache.h"

#import "LXRTopicModel.h"
#import "LXRCommentModel.h"
#import "LXRUserModel.h"

#import "LXRTopicPictureView.h"
#import "LXRTopicVoiceView.h"
#import "LXRTopicVideoView.h"

@interface LXRTopicCell ()
/**头像*/
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
/**昵称*/
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
/**时间*/
@property (weak, nonatomic) IBOutlet UILabel *createTimeLable;
/**顶*/
@property (weak, nonatomic) IBOutlet UIButton *dingButton;
/**踩*/
@property (weak, nonatomic) IBOutlet UIButton *caiButton;
/**分享*/
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
/**评论*/
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
/**新浪加V*/
@property (weak, nonatomic) IBOutlet UIImageView *sinaVView;
/**帖子文字内容*/
@property (weak, nonatomic) IBOutlet UILabel *text_Lable;

/**图片帖子中间的内容View*/
@property(nonatomic,weak)LXRTopicPictureView* pictureView;
/**声音帖子中间的内容View*/
@property(nonatomic,weak)LXRTopicVoiceView* voiceView;
/**视频帖子中间的内容View*/
@property(nonatomic,weak)LXRTopicVideoView* videoView;

/**最热评论内容*/
@property (weak, nonatomic) IBOutlet UILabel *topCmtContentLable;
/**最热评论整体View*/
@property (weak, nonatomic) IBOutlet UIView *topCmtView;

@end

@implementation LXRTopicCell
#pragma mark - 懒加载
-(LXRTopicPictureView *)pictureView{
    
    if (_pictureView == nil) {
        LXRTopicPictureView* pictureView = [LXRTopicPictureView viewFromXib];
        [self.contentView addSubview:pictureView];
        _pictureView = pictureView;
    }
    return _pictureView;
}
-(LXRTopicVoiceView *)voiceView{
    
    if (_voiceView == nil) {
        LXRTopicVoiceView* voiceView = [LXRTopicVoiceView viewFromXib];
        [self.contentView addSubview:voiceView];
        _voiceView = voiceView;
    }
    return _voiceView;
}
-(LXRTopicVideoView *)videoView{
    if (_videoView == nil) {
        LXRTopicVideoView* videoView = [LXRTopicVideoView viewFromXib];
        [self.contentView addSubview:videoView];
        _videoView = videoView;
    }
    return _videoView;
}

/**************************************************/


- (void)awakeFromNib {
    UIImageView* bgView = [[UIImageView alloc]init];
    bgView.image = [UIImage imageNamed:@"mainCellBackground"];
    self.backgroundView = bgView;
    
    
    
    
}
/**重新设置Frame*/
-(void)setFrame:(CGRect)frame{
    
    //设置  版本更新
    //frame.origin.x = LXRTopicCellMargin;
    //frame.size.width = frame.size.width - 2*LXRTopicCellMargin;
    
//    frame.size.height -= LXRTopicCellMargin;
    //重点!!!这么写可以防止以后别的界面不停的调用setFrame造成Frame不停的减
    frame.size.height = self.model.cellHeight - LXRTopicCellMargin;
    
    frame.origin.y += LXRTopicCellMargin;
    
    [super setFrame:frame];
}
/** 创建弹框控制器 */
- (IBAction)moreClick {
    //创建弹框控制器
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //添加行为标题
    [alert addAction:[UIAlertAction actionWithTitle:@"收藏" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        LXRLog(@"%@",action.title);
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        LXRLog(@"%@",action.title);
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        LXRLog(@"%@",action.title);
    }]];
    //显示弹框
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];

}


/**模型->控件赋值*/
-(void)setModel:(LXRTopicModel *)model{
    _model = model;
    
    //新浪加V
    self.sinaVView.hidden = !model.sina_v;
    
    //设置名字
    self.nameLable.text = model.name;

    //设置圆形头像
    [self.profileImageView setCircleHeader:model.profile_image];
    
    //设置帖子的创建时间
    self.createTimeLable.text = model.create_time;
    
    //设置按钮文字内容
    [self setupButtonTitle:self.dingButton count:model.ding placeholder:@"顶"];
    [self setupButtonTitle:self.caiButton count:model.cai placeholder:@"踩"];
    [self setupButtonTitle:self.shareButton count:model.repost placeholder:@"分享"];
    [self setupButtonTitle:self.commentButton count:model.comment placeholder:@"评论"];
    //设置文字
    self.text_Lable.text = model.text;
    
    //根据模型类型(帖子类型)添加对应的内容到cell的中间
    if (model.type == LXRTopicTypePicture) {//图片帖子
        //设置需要隐藏的控件
        self.pictureView.hidden = NO;
        self.voiceView.hidden =YES;
        self.videoView.hidden = YES;
        
        self.pictureView.model = model;
        self.pictureView.frame = model.pictureF;
        
    }else if (model.type == LXRTopicTypeVoice){
        //设置需要隐藏的控件
        self.voiceView.hidden = NO;
        self.pictureView.hidden =YES;
        self.videoView.hidden = YES;
        
        self.voiceView.model = model;
        self.voiceView.frame = model.voiceF;
    }else if (model.type == LXRTopicTypeVideo){
        //设置需要隐藏的控件
        self.videoView.hidden = NO;
        self.pictureView.hidden =YES;
        self.voiceView.hidden = YES;
        
        self.videoView.model = model;
        self.videoView.frame = model.videoF;
    }else{
        self.videoView.hidden = YES;
        self.pictureView.hidden =YES;
        self.voiceView.hidden = YES;
    }
    
    //处理最热评论
    LXRCommentModel* cmt = [model.top_cmt firstObject];
    if (cmt) {
        self.topCmtView.hidden = NO;
        self.topCmtContentLable.text = [NSString stringWithFormat:@"%@ : %@", cmt.user.username, cmt.content];;
    }else{
        self.topCmtView.hidden = YES;
    }
}



/**设置按钮标题,数量显示*/
-(void)setupButtonTitle:(UIButton*)button count:(NSInteger)count placeholder:(NSString*)placeholder{
    //判断Button显示文字
     if (count > 10000) {
        placeholder = [NSString stringWithFormat:@"%.1f万",count/10000.0];
    }else if(count >0){
        placeholder = [NSString stringWithFormat:@"%zd",count];
    }
    [button setTitle:placeholder forState:UIControlStateNormal];

}

/**比较时间方法*/
-(void)testDate:(NSString*)create_time{
    //日期格式化类
    NSDateFormatter* fmt = [[NSDateFormatter alloc]init];
    //设置日期格式(y:年,M:月,d:天 H:24消失,小写h:12小时,m:分钟,s:秒)
    fmt.dateFormat = @"yyyy-MM-dd HH-mm-ss";
    
    //当前时间
    //NSDate* now = [NSDate date];
    //发帖时间
    //NSDate* create = [fmt dateFromString:create_time];
    
    //LXRLog(@"%@",[now deltaFrom:create]);
    
    
    //日历
    //    NSCalendar* calendar = [NSCalendar currentCalendar];
    //比较时间
    //    NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    //    NSDateComponents* cmps = [calendar components:unit fromDate:create toDate:now options:0];
    //    LXRLog(@"%@ %@",create,now);
    //    LXRLog(@"%zd %zd %zd %zd %zd %zd",cmps.year,cmps.month,cmps.day,cmps.hour,cmps.minute,cmps.second);
    //获得NSDate的每一个元素
    //    NSInteger year = [calendar component:NSCalendarUnitYear fromDate:now];
    //    NSInteger month = [calendar component:NSCalendarUnitMonth fromDate:now];
    //    NSInteger day = [calendar component:NSCalendarUnitDay fromDate:now];
    //
    //    NSDateComponents* cmps = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
}

@end
