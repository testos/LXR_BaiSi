//
//  LXRCommentModel.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/27.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
@class LXRUserModel;
@interface LXRCommentModel : NSObject
/**id*/
@property(nonatomic,copy)NSString* ID;
/**用户*/
@property(nonatomic,strong)LXRUserModel* user;

/**评论内容*/
@property(nonatomic,copy)NSString* content;
/**被点赞的数量*/
@property(nonatomic,assign)NSInteger like_count;

/**音频评论文件的时长*/
@property(nonatomic,assign)NSInteger voicetime;
/**音频评论文件路径*/
@property(nonatomic,copy)NSString* voiceuri;

@end
