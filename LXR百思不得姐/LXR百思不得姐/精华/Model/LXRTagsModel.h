//
//  LXRTagsModel.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/12.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LXRTagsModel : NSObject
/**图标*/
@property(nonatomic,copy)NSString* image_list;
/**名字*/
@property(nonatomic,copy)NSString* theme_name;
/**关注数*/
@property(nonatomic)NSInteger sub_number;


@end
