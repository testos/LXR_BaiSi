//
//  LXRTopicModel.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/17.
//  Copyright © 2016年 mac. All rights reserved.


//帖子模型

#import "LXRTopicModel.h"
#import "LXRCommentModel.h"
#import "LXRUserModel.h"

#import "MJExtension.h"

@interface LXRTopicModel ()
{
    CGFloat _cellHeight;
}
@end

@implementation LXRTopicModel
/**转服务器传回来的key 进行自定义修改*/
+(NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"small_image" : @"image0",
             @"middle_image" : @"image1",
             @"large_image" : @"image2",
             @"ID" : @"id"
             };
}
/**把数组转成自定义类型模型,如果模型套套型,一并转完*/
+(NSDictionary *)mj_objectClassInArray{
    return @{@"top_cmt" : @"LXRCommentModel"};
}

/**比较创建时间和目前的时间结果*/
-(NSString *)create_time{

    //日期格式化
    NSDateFormatter* fmt = [[NSDateFormatter alloc]init];
    //设置日期格式
    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    //帖子创建时间
    NSDate* create = [fmt dateFromString:_create_time];
    
    if (create.isThisYear) {//今年
        if (create.isToday) {//今天
            //取得现在时间和发帖时间之间的差值
            NSDateComponents* cmps = [[NSDate date] deltaFrom:create];
            if (cmps.hour >= 1) {//时间差距大于1小时
                return [NSString stringWithFormat:@"%zd小时前",cmps.hour];
            }else if (cmps.minute >= 1){//时间差距大于1分钟
                return [NSString stringWithFormat:@"%zd分钟前",cmps.minute];
            }else{//时间差距小于1分钟
                return @"刚刚";
            }
        }else if(create.isYesterday){//昨天
            fmt.dateFormat = @"昨天 HH:mm:ss";
            return [fmt stringFromDate:create];
        }else{//其他
            fmt.dateFormat = @"MM-dd HH:mm:ss";
            return [fmt stringFromDate:create];
        }
    }else{//不是今年
        //设置时间
        return _create_time;
    }

}

/**cell的高度*/
-(CGFloat)cellHeight{
    //判断cellHeight,只计算一次高度,重要!!!
    if(!_cellHeight){
        
        //文字的最大尺寸
        CGSize maxSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 2*LXRTopicCellMargin, MAXFLOAT);
        
        //文字的宽度
        CGFloat textH = [self.text boundingRectWithSize:maxSize
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]} context:nil].size.height;
        //cell的高度 (当只有text的时候cell的高度)
        _cellHeight = LXRTopicCellTextY + textH + LXRTopicCellMargin;
        
        //根据段子的类型计算cell的高度
        if (self.type == LXRTopicTypePicture) {    //图片帖子
            //图片显示出来的宽度
            CGFloat pictureW = maxSize.width;
            //图片显示出来的高度  交叉相乘 
            CGFloat pictureH = pictureW*self.height/self.width;
            if (pictureH >= LXRTopicCellPictureMaxH) {//图片高度过长
                pictureH = LXRTopicCellPictureBreakH;
                self.bigPicture = YES;
            }
            //计算图片控件的Frame
            CGFloat pictureX  = LXRTopicCellMargin;
            CGFloat pictureY = LXRTopicCellTextY + textH + LXRTopicCellMargin;
            _pictureF = CGRectMake(pictureX, pictureY, pictureW, pictureH);
            //cell的高度(当有图片控件的时候cell的高度==图片控件的高度+统一间距值)
            _cellHeight += pictureH + LXRTopicCellMargin;
            
        }else if (self.type == LXRTopicTypeVoice){  //声音帖子
            CGFloat voiceX = LXRTopicCellMargin;
            CGFloat voiceY = LXRTopicCellTextY + textH + LXRTopicCellMargin;
            CGFloat voiceW = maxSize.width;
            CGFloat voiceH = voiceW*self.height/self.width;
            _voiceF = CGRectMake(voiceX, voiceY, voiceW, voiceH);
            //cell的高度(当有图片控件的时候cell的高度==图片控件的高度+统一间距值)
            _cellHeight += voiceH + LXRTopicCellMargin;
            
        }else if (self.type == LXRTopicTypeVideo){
            CGFloat videoX = LXRTopicCellMargin;
            CGFloat videoY = LXRTopicCellTextY + textH + LXRTopicCellMargin;
            CGFloat videoW = maxSize.width;
            CGFloat videoH = videoW*self.height/self.width;
            _videoF = CGRectMake(videoX, videoY, videoW, videoH);
            //cell的高度(当有图片控件的时候cell的高度==图片控件的高度+统一间距值)
            _cellHeight += videoH + LXRTopicCellMargin;
        }
        
        // 如果有最热评论
        LXRCommentModel *cmt = [self.top_cmt firstObject];
        if (cmt) {
            NSString *content = [NSString stringWithFormat:@"%@ : %@", cmt.user.username, cmt.content];
            //计算评论内容的高度--->根据字体大小和maxSize大小计算
            CGFloat contentH = [content boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]} context:nil].size.height;
            
            //LXRTopicCellTopCmtTitleH最热评论文字高度+评论内容高度+间距
            _cellHeight += LXRTopicCellTopCmtTitleH + contentH + LXRTopicCellMargin;
        }
        
        //cell的最终高度 (文字+图片+底部工具条)
        _cellHeight += LXRTopicCellBottomBarH + LXRTopicCellMargin;
    }
    
    
    return _cellHeight;
}


@end
