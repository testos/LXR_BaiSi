//
//  LXRTagsController.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/12.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRTagsController.h"
#import "LXRTagsModel.h"
#import "LXRTagsCell.h"
#import "LXRHeadWindow.h"

#import "MBProgressHUD+MJ.h"
#import "MJExtension.h"
#import "LXRHTTPSessionManager.h"

@interface LXRTagsController ()
/** 用户数据 */
@property(nonatomic,strong)NSArray* Tags;

@end

//标识
static NSString* const LXRTagsId = @"tag";

@implementation LXRTagsController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //显示Window
    [LXRHeadWindow show];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //初始化设置
    [self setupTableView];
    //请求数据
    [self loadTags];
}
/**
 *  初始化设置
 */
-(void)setupTableView{
    //设置cell行高
    self.tableView.rowHeight = 70;
    //取消cell系统分割线
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //设置tableView背景颜色
    self.tableView.backgroundColor = LXR_BJ_Color;
    
    self.title = @"推荐标签";
    //注册
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([LXRTagsCell class]) bundle:nil] forCellReuseIdentifier:LXRTagsId];
}

/**
 *  请求数据
 */
-(void)loadTags{
    //发送请求
    [MBProgressHUD showMessage:@"数据加载中"];
    //请求参数
    NSMutableDictionary* params = [NSMutableDictionary new];
    params[@"a"] = @"tag_recommend";
    params[@"c"] = @"topic";
    params[@"action"] = @"sub";
    
    LXRWeakSelf;
    [[LXRHTTPSessionManager manager] GET:@"http://api.budejie.com/api/api_open.php" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [MBProgressHUD hideHUD];
        //LXRLog(@"%@",responseObject);
        
        weakSelf.Tags = [LXRTagsModel mj_objectArrayWithKeyValuesArray:responseObject];
        
        //刷新tableView数据
        [weakSelf.tableView reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD showError:@"数据加载失败!!!"];
    }];

}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.Tags.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LXRTagsCell *cell = [tableView dequeueReusableCellWithIdentifier:LXRTagsId];
    
    cell.tagModel = self.Tags[indexPath.row];
    
    return cell;
}




@end
