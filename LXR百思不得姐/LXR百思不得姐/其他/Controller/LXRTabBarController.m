//
//  LXRTabBarController.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/6.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRTabBarController.h"
#import "LXREssenceViewController.h"
#import "LXRNewViewController.h"
#import "LXRFriendTrendsViewController.h"
#import "LXRMeViewController.h"
#import "LXRTabBar.h"
#import "LXRNavController.h"

@interface LXRTabBarController ()

@end

@implementation LXRTabBarController
#pragma mark - 设置主题 这个方法只调用一次
+(void)initialize{
    //通过appearance同一设置所有UITabBarItem的文字属性
    NSMutableDictionary* attrs = [NSMutableDictionary new];
    //文字 字体大小
    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:12];
    //文字 Foreground前景颜色
    attrs[NSForegroundColorAttributeName] = [UIColor grayColor];
    
    NSMutableDictionary* selectesAttrs = [NSMutableDictionary new];
    selectesAttrs[NSFontAttributeName] = attrs[NSFontAttributeName];
    selectesAttrs[NSForegroundColorAttributeName] = [UIColor darkGrayColor];
    
    
    UITabBarItem* item = [UITabBarItem appearance];
    [item setTitleTextAttributes:attrs forState:UIControlStateNormal];
    [item setTitleTextAttributes:selectesAttrs forState:UIControlStateSelected];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //添加子控制器
    [self AddController];
    
    
}

#pragma mark - 添加子控制器
-(void)AddController{
    
    [self setupChildVc:[[LXREssenceViewController alloc]init] Title:@"精华" Image:@"tabBar_essence_icon" SelectedImage:@"tabBar_essence_click_icon"];
    
    [self setupChildVc:[[LXRNewViewController alloc]init]Title:@"新帖" Image:@"tabBar_new_icon" SelectedImage:@"tabBar_new_click_icon"];
    
    [self setupChildVc:[[LXRFriendTrendsViewController alloc]init] Title:@"关注" Image:@"tabBar_friendTrends_icon" SelectedImage:@"tabBar_friendTrends_click_icon"];
    
    [self setupChildVc:[[LXRMeViewController alloc]initWithStyle:UITableViewStyleGrouped] Title:@"我" Image:@"tabBar_me_icon" SelectedImage:@"tabBar_me_click_icon"];
    
    //添加发布按钮
    //如果值是readonly 尝试用KVC进行赋值
    [self setValue:[[LXRTabBar alloc] init] forKeyPath:@"tabBar"];
}

#pragma mark - 初始化子控制器
-(void) setupChildVc:(UIViewController*)vc Title:(NSString*)title Image:(NSString*)image SelectedImage:(NSString*)selectedImage{
   //设置文字和图片
    vc.tabBarItem.title = title;
    vc.tabBarItem.image = [UIImage imageNamed:image];
    vc.tabBarItem.selectedImage = [UIImage imageNamed:selectedImage];

    //包装一个导航控制器,添加导航控制器tabbarController的子控制器
    LXRNavController* nav = [[LXRNavController alloc]initWithRootViewController:vc];
    
    [self addChildViewController:nav];

}

@end
