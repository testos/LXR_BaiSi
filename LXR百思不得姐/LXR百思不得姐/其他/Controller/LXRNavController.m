//
//  LXRNavController.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/7.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRNavController.h"

@interface LXRNavController ()<UIGestureRecognizerDelegate>

@end

@implementation LXRNavController
//当第一次使用这个类的时候会调用一次
+(void)initialize{
    //统一设置NavigationBar背景图片
    //appearanceWhenContainedIn 当使用LXRNavController当UINavigationBar的时候才设置图片为navigationbarBackgroundWhite
    UINavigationBar* bar = [UINavigationBar appearanceWhenContainedIn:[self class], nil];
    NSMutableDictionary* BarAttrs = [NSMutableDictionary new];
    BarAttrs[NSFontAttributeName] = [UIFont boldSystemFontOfSize:18];
    //设置导航栏标题字体大小
    [bar setTitleTextAttributes:BarAttrs];
    //背景图片
    [bar setBackgroundImage:[UIImage imageNamed:@"navigationbarBackgroundWhite"] forBarMetrics:UIBarMetricsDefault];
    
    
    //统一设置NavigationBarItem
    UIBarButtonItem* item = [UIBarButtonItem appearance];
    //UIControlStateNormal-->设置导航栏文字主题设置(字体大小和颜色)
    NSMutableDictionary* ItemAttrs = [NSMutableDictionary dictionary];
    ItemAttrs[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    ItemAttrs[NSForegroundColorAttributeName] = [UIColor blackColor];
    [item setTitleTextAttributes:ItemAttrs forState:UIControlStateNormal];
    //UIControlStateDisabled-
    NSMutableDictionary* ItemDisabledAttrs = [NSMutableDictionary dictionary];
    ItemDisabledAttrs[NSForegroundColorAttributeName] = [UIColor lightGrayColor];
    ItemDisabledAttrs[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    [item setTitleTextAttributes:ItemDisabledAttrs forState:UIControlStateDisabled];
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //添加交互式流行手势 监听返回效果
    self.interactivePopGestureRecognizer.delegate = self;

}

/**
 *  手势识别器即将开始
 */
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if (self.childViewControllers.count == 1) {
        return NO;
    }
    return YES;
}

/** 
 *  可以在这个方法中拦截所有PUSH进来的控制器
 */
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    //统一设置控制器返回按钮的文字
    if (self.childViewControllers.count > 0) { //如果push进来的不是第一个控制器
        UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        //设置文字 颜色
        [button setTitle:@"返回" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    
        //设置按钮图片
        [button setImage:[UIImage imageNamed:@"navigationButtonReturn"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"navigationButtonReturnClick"] forState:UIControlStateHighlighted];
        
        //设置按钮大小,一定要设置大小,不然显示不出来
        button.size = CGSizeMake(60, 30);
        
        //让按钮内容的所有内容左对齐
        //button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        //根据大小填充 建议使用
        [button sizeToFit];
        
        //设置按钮贴着屏幕左边  (上左下右) ---------重点!!!
        button.contentEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
        
        
        //添加点击事件  返回界面功能
        [button addTarget:self action:@selector(Back) forControlEvents:UIControlEventTouchUpInside];
        
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button];
        //当push下一界面的时候,隐藏tabBar
        viewController.hidesBottomBarWhenPushed = YES;
    }
    
    //这句super的push要放在后面,让viewController可以覆盖上面设置的leftBarButtonItem
    [super pushViewController:viewController animated:animated];
    

    
}
#pragma mark - 实现返回操作
-(void)Back{
    [self popViewControllerAnimated:YES];
}
@end
