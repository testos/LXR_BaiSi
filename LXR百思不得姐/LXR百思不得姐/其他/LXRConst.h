


#import <UIKit/UIKit.h>
/**帖子数据类型*/
typedef enum{
    LXRTopicTypeAll = 1,        //全部
    LXRTopicTypePicture = 10,   //图片
    LXRTopicTypeWord = 29,      //段子
    LXRTopicTypeVoice = 31,     //声音
    LXRTopicTypeVideo = 41      //视频
}LXRTopicType;

/**tabBar被选中的通知名字*/
UIKIT_EXTERN NSString* const LXRTabBarDidSelectNotification;
/**tabBar被选中的通知->被选中的控制器的Index key*/
UIKIT_EXTERN NSString* const LXRSelectedControllerIndexKey;
/**tabBar被选中的通知->被选中的控制器的key*/
UIKIT_EXTERN NSString* const LXRSelectedControllerKey;

/**************************************************************/

/**标签-间距*/
UIKIT_EXTERN CGFloat const LXRTagMargin;
/**标签-高度*/
UIKIT_EXTERN CGFloat const LXRTagH;

/**************************************************************/

/**精华-顶部标题的高度*/
UIKIT_EXTERN CGFloat const LXRTitlesViewH;
/**精华-顶部标题的Y*/
UIKIT_EXTERN CGFloat const LXRTitlesViewY;
/**精华-cell-间距*/
UIKIT_EXTERN CGFloat const LXRTopicCellMargin;
/**精华-cell-文字内容的Y值*/
UIKIT_EXTERN CGFloat const LXRTopicCellTextY;
/**精华-cell-顶部工具条的高度*/
UIKIT_EXTERN CGFloat const LXRTopicCellBottomBarH;

/**精华-cell-图片帖子的最大高度*/
UIKIT_EXTERN CGFloat const LXRTopicCellPictureMaxH;
/**精华-cell-图片帖子一旦超过最大高度,就用Break高度*/
UIKIT_EXTERN CGFloat const LXRTopicCellPictureBreakH;

/**LXRUserModel模型-->性别属性值*/
UIKIT_EXTERN NSString* const LXRUserSexMale;    //男
UIKIT_EXTERN NSString* const LXRUserSexFemale;  //女

/** 精华-cell-最热评论标题的高度 */
UIKIT_EXTERN CGFloat const LXRTopicCellTopCmtTitleH;

