//
//  LXRHeadWindow.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/29.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LXRHeadWindow : NSObject
/**头部Window显示*/
+(void)show;
/**头部Window隐藏*/
+(void)hide;

@end
