//
//  LXRPushGuideView.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/15.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRPushGuideView.h"

@implementation LXRPushGuideView

+(void)show{
    //打印info里系统版本号
    //LXRLog(@"%@",[NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"]);
    //取出系统Window
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    
    NSString* key = @"CFBundleShortVersionString";
    //取出 当前 软件版本号
    NSString* currentVersion = [NSBundle mainBundle].infoDictionary[key];
    //获得 沙盒 中存储的版本号
    NSString* sanboxVersion = [[NSUserDefaults standardUserDefaults] stringForKey:key];
    
    //判断当前版本号和存储版本号
    if (![currentVersion isEqualToString:sanboxVersion]) {
        LXRPushGuideView* guideView = [LXRPushGuideView viewFromXib];
        guideView.frame = [UIScreen mainScreen].bounds;
        [window addSubview:guideView];
        //存储版本号
        [[NSUserDefaults standardUserDefaults]setObject:currentVersion forKey:key];
        //马上写入NSUserDefaults文件里
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}


- (IBAction)Close:(id)sender {
    [self removeFromSuperview];
}

@end
