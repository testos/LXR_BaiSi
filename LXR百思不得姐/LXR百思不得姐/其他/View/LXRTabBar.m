//
//  LXRTabBar.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/6.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRTabBar.h"
#import "LXRPublishViewController.h"
@interface LXRTabBar ()

@property(nonatomic,weak)UIButton* PublishButton;

@end
@implementation LXRTabBar

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //设置TabBar背景图片
        [self setBackgroundImage:[UIImage imageNamed:@"tabbar-light"]];
        //添加发布按钮
        UIButton* publishButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //设置发布按钮背景图片
        [publishButton setBackgroundImage:[UIImage imageNamed:@"tabBar_publish_icon"]forState:UIControlStateNormal];
        [publishButton setBackgroundImage:[UIImage imageNamed:@"tabBar_publish_click_icon"] forState:UIControlStateHighlighted];
        [publishButton addTarget:self action:@selector(publishClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:publishButton];
        self.PublishButton = publishButton;
         
    }
    return self;
}
-(void)publishClick{
    LXRPublishViewController* vc = [[LXRPublishViewController alloc]init];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:NO completion:nil];
}


-(void)layoutSubviews{

    [super layoutSubviews];
    
//    self.PublishButton.Width = self.PublishButton.currentBackgroundImage.size.width;
//    self.PublishButton.Height = self.PublishButton.currentBackgroundImage.size.height;
    self.PublishButton.size = self.PublishButton.currentBackgroundImage.size;
    
    //设置发布按钮的Frame
    self.PublishButton.center = CGPointMake(self.width*0.5, self.height*0.5);
    
    //设置其他UITabBarButton的Frame
    CGFloat buttonY = 0;
    CGFloat buttonW = self.width/5;
    CGFloat buttonH = self.height;
    NSInteger index = 0;
    for (UIView* button in self.subviews) {
        //如果button不是UITabBarButton类型就跳过continue
        if (![button isKindOfClass:NSClassFromString(@"UITabBarButton")]) continue;
        
        CGFloat buttonX = buttonW*((index>1)?(index+1):index);
        button.frame = CGRectMake(buttonX, buttonY, buttonW, buttonH);
        
        index++;
    }
    
}
@end
