//
//  UIImageView+LXRExtension.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/30.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (LXRExtension)
/** 设置圆形头像 */
-(void)setCircleHeader:(NSString*)url;
@end
