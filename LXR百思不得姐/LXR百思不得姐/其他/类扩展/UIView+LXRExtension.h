//
//  UIView+LXRExtension.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/7.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (LXRExtension)

/**快捷设置Frame参数*/
@property(nonatomic,assign)CGFloat x;
@property(nonatomic,assign)CGFloat y;
@property(nonatomic,assign)CGFloat width;
@property(nonatomic,assign)CGFloat height;
@property(nonatomic,assign)CGFloat centerX;
@property(nonatomic,assign)CGFloat centerY;
@property(nonatomic,assign)CGSize size;

/**判断一个控件是否真正显示在主窗口*/
-(BOOL)isShowingOnKeyWindow;
/**读xib方法*/
+(instancetype)viewFromXib;
@end
