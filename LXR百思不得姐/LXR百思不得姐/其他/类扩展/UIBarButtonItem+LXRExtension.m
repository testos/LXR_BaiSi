//
//  UIBarButtonItem+LXRExtension.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/7.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "UIBarButtonItem+LXRExtension.h"

@implementation UIBarButtonItem (LXRExtension)
/**
 *  创建BarButtonItem
 *
 *  @param image     普通状态图片
 *  @param highImage 高亮状态图片
 *  @param action    点击方法
 */
+(instancetype)itemWithImage:(NSString *)image highImage:(NSString *)highImage target:(id)target action:(SEL)action{
    //创建自定义类型按钮
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    //设置状态图片
    [button setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:highImage] forState:UIControlStateHighlighted];
    //设置按钮的Size等于当前图片的Size
    button.size= button.currentBackgroundImage.size;
    //添加点击时间  方法名:action
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    //返回UIBarButtonItem创建
    return [[self alloc]initWithCustomView:button];

}
@end
