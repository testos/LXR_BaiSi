//
//  UIImage+LXRExtension.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/30.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "UIImage+LXRExtension.h"

@implementation UIImage (LXRExtension)
/**
 *  圆形图片
 */
-(UIImage *)circleImage{
    //NO代表透明
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0);
    //获得上下文
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    //添加一个圆
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextAddEllipseInRect(ctx, rect);
    //裁剪
    CGContextClip(ctx);
    //将图片画上去
    [self drawInRect:rect];
    //从当前上下文ctx获得图片--->赋值-->image
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    //关闭上下文
    UIGraphicsEndImageContext();
    return image;
}

@end
