//
//  UIView+LXRExtension.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/7.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "UIView+LXRExtension.h"

@implementation UIView (LXRExtension)
/**判断一个控件是否真正显示在主窗口*/
-(BOOL)isShowingOnKeyWindow{
    //主窗口
    UIWindow* keyWindow = [UIApplication sharedApplication].keyWindow;
    //以窗口左上角为坐标原点,计算self的矩形框
    CGRect newFrame = [keyWindow convertRect:self.frame fromView:self.superview];
    CGRect WindowBounds = keyWindow.bounds;
    //主窗口的Bounds和self的矩形框 是否有重叠
    BOOL intersects = CGRectIntersectsRect(newFrame, WindowBounds);
    
    return !self.isHidden && self.alpha > 0.01 && self.window == keyWindow && intersects;
}
/**读xib方法*/
+(instancetype)viewFromXib{
    return [[[NSBundle mainBundle]loadNibNamed:NSStringFromClass(self) owner:nil options:nil]lastObject];
}

#pragma mark - Set方法
-(void)setX:(CGFloat)x{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}
-(void)setY:(CGFloat)y{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}
-(void)setWidth:(CGFloat)width{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}
-(void)setHeight:(CGFloat)height{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}
-(void)setSize:(CGSize)size{

    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}
-(void)setCenterX:(CGFloat)centerX{
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}
-(void)setCenterY:(CGFloat)centerY{
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}
#pragma mark - Get方法
-(CGSize)size{
    return self.frame.size;
}
-(CGFloat)x{
    return self.frame.origin.x;
}
-(CGFloat)y{
    return self.frame.origin.y;
}
-(CGFloat)height{
    return self.frame.size.height;
}
-(CGFloat)width{
    return self.frame.size.width;
}
-(CGFloat)centerX{
    return  self.center.x;
}
-(CGFloat)centerY{
    return  self.center.y;
}

@end
