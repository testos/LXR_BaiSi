//
//  NSDate+LXRExtension.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/22.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "NSDate+LXRExtension.h"

@implementation NSDate (LXRExtension)
/**
 *  比较from和self时间差值
 */
-(NSDateComponents *)deltaFrom:(NSDate *)from{
    //日历
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    //比较时间
    NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    
    return [calendar components:unit fromDate:from toDate:self options:0];
}

/**
 *是否是今年
 */
-(BOOL)isThisYear{
    //日历
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSInteger nowYear = [calendar component:NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger selfYear = [calendar component:NSCalendarUnitYear fromDate:self];
    return nowYear == selfYear;
}

/**
 *是否是今天
 */
-(BOOL)isToday{
    //日期格式化类
    NSDateFormatter* fmt = [[NSDateFormatter alloc]init];
    fmt.dateFormat = @"yyyy-MM-dd";
    //时间字符串
    NSString* nowString = [fmt stringFromDate:[NSDate date]];
    NSString* selfString = [fmt stringFromDate:self];
    
    return [nowString isEqualToString:selfString];
    
}

/**+(BOOL)isToday{
    //日历
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay;
    //年月日
    NSDateComponents* nowCmps = [calendar components:unit fromDate:[NSDate date]];
    NSDateComponents* selfCmps = [calendar components:unit fromDate:[self date]];
    
    return nowCmps.year == selfCmps.year
    &&nowCmps.month == selfCmps.month
    &&nowCmps.day == selfCmps.day;
    
}*/

/**
 *是否是昨天
 */
-(BOOL)isYesterday{

    //2014-12-31 23:59:59 -> 2014-12-31
    //2015-01-01 00:00:01 -> 2015-01-01
    
    //日期格式化
    NSDateFormatter* fmt = [[NSDateFormatter alloc]init];
    fmt.dateFormat = @"yyyy-MM-dd";
    
    NSDate* nowDate = [fmt dateFromString:[fmt stringFromDate:[NSDate date]]];
    NSDate* selfDate = [fmt dateFromString:[fmt stringFromDate:self]];
    
    //日历
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* cmps = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:selfDate toDate:nowDate options:0];
    return cmps.year == 0
    &&cmps.month ==0
    &&cmps.day == 1;
}



@end
