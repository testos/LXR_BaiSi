//
//  LXRLoginRegisterViewController.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/13.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRLoginRegisterViewController.h"
#import "LXRHeadWindow.h"

@interface LXRLoginRegisterViewController ()
@property (weak, nonatomic) IBOutlet UITextField *PhoneField;
@property (weak, nonatomic) IBOutlet UITextField *PwdField;
/**登录框距离控制器view左边的间距*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginViewLeftMargin;

@end

@implementation LXRLoginRegisterViewController

- (IBAction)back {
    //当返回时,恢复状态栏默认设置
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    //恢复LXRHeadWindow
    [LXRHeadWindow show];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //状态栏显示为白色
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    //隐藏LXRHeadWindow
    [LXRHeadWindow hide];
    

}

- (IBAction)showLoginBtn:(UIButton *)sender {
    //退出键盘
    [self.view endEditing:YES];
    
    if (self.loginViewLeftMargin.constant == 0) {
        self.loginViewLeftMargin.constant = - self.view.frame.size.width;
        [sender setTitle:@"已有账号?" forState:UIControlStateNormal];
    }else{
        
        self.loginViewLeftMargin.constant = 0;
        [sender setTitle:@"注册账号?" forState:UIControlStateNormal];
    }
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

//测试用
-(void)test{
    //设置按钮四周圆角
        //self.LoginBtn.layer.cornerRadius = 10;
       // self.LoginBtn.layer.masksToBounds = YES;
    //文字属性
    NSMutableDictionary* attrs = [NSMutableDictionary dictionary];
    attrs[NSForegroundColorAttributeName] = [UIColor whiteColor];
    
    //NSAttributedString:带有属性的文字(富文本技术)
    NSAttributedString* placeholder = [[NSAttributedString alloc]initWithString:@"手机号" attributes:attrs];
    
    //改变UITextField占位文字属性设置
    self.PhoneField.attributedPlaceholder = placeholder;
}

/**当前控制器状态栏是白色*/
//-(UIStatusBarStyle)preferredStatusBarStyle{
//
//    return UIStatusBarStyleLightContent;
//}

@end
