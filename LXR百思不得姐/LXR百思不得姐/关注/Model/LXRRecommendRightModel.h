//
//  LXRRecommendRightModel.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/11.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LXRRecommendRightModel : NSObject
/**头像*/
@property(nonatomic,copy)NSString* header;
/**粉丝数*/
@property(nonatomic)NSInteger fans_count;
/**昵称*/
@property(nonatomic,copy)NSString* screen_name;



@end
