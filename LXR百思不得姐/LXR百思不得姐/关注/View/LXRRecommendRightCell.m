//
//  LXRRecommendRightCell.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/10.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRRecommendRightCell.h"
#import "LXRRecommendRightModel.h"
#import "UIImageView+WebCache.h"
@interface LXRRecommendRightCell()
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *screenNameLable;
@property (weak, nonatomic) IBOutlet UILabel *fansCountLable;

@end

@implementation LXRRecommendRightCell

-(void)setModel:(LXRRecommendRightModel *)model{
    _model = model;
    
    //设置圆形头像
    [self.headerImageView setCircleHeader:model.header];
    
    self.screenNameLable.text = model.screen_name;
    
    NSString *fansCount = nil;
    if (model.fans_count < 10000) { //粉丝人数少于10000,正常显示
        fansCount = [NSString stringWithFormat:@"%zd人关注",model.fans_count];
    }else{  //粉丝数大于10000,处理后显示
        fansCount = [NSString stringWithFormat:@"%.1f万人订阅",model.fans_count/10000.0];
    }
    self.fansCountLable.text = fansCount;
}



@end
