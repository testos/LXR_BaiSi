//
//  LXRRecommendLeftModel.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/8.
//  Copyright © 2016年 mac. All rights reserved.


//推荐关注---->左边数据模型

#import "LXRRecommendLeftModel.h"
#import "MJExtension.h"


@implementation LXRRecommendLeftModel
/**把系统返回的id重新命名新的ID*/
/**第一种方法*/
+(NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"ID" : @"id"};
}
/**第二种方法*/
//+(NSString *)mj_replacedKeyFromPropertyName121:(NSString *)propertyName{
//    
//    if ([propertyName isEqualToString:@"id"]) return @"ID";
//    return propertyName;
//}


-(NSMutableArray *)Rights{
    
    if (_Rights == nil) {
        _Rights = [NSMutableArray array];
    }
    return _Rights;
}

@end
