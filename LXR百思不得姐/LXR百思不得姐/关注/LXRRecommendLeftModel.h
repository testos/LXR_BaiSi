//
//  LXRRecommendLeftModel.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/8.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LXRRecommendLeftModel : NSObject

/**id*/
@property(nonatomic)NSInteger ID;
/**总数*/
@property(nonatomic)NSInteger count;
/**姓名*/
@property(nonatomic,copy)NSString* name;



/**这个类别对应的用户数据,储存模型数据,用于维护数据*/
@property(nonatomic,strong)NSMutableArray* Rights;
/**总数*/
@property(nonatomic)NSInteger total;
/**当前页码*/
@property(nonatomic)NSInteger currentPage;
@end
